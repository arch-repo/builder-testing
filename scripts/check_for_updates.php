#!/usr/bin/php
<?php

$packages = [];

$args = [
	'v' => 5,
	'type' => 'info',
	'arg' => []
];

$errors = [];


$package_path = dirname(__DIR__).'/packages/';
if ($handle = opendir($package_path)) {
	while (false !== ($entry = readdir($handle))) {
		if ($entry != "." && $entry != "..") {
			$package = json_decode(file_get_contents($package_path.$entry), true);
			if (!empty($package['name'])) {
				$packages[$package['name']] = $package;
			}
		}
	}
	closedir($handle);
}

foreach ($packages as $package_base => $package) {
	$args['arg'][] = $package_base;
}

$query = http_build_query($args);

$aurweb = file_get_contents('https://aur.archlinux.org/rpc/?'.$query);
$aur = json_decode($aurweb, true);
if (isset($aur['results'])) {
	foreach ($aur['results'] as $package) {
		$packages[$package['Name']]['aur'] = $package;
	}
}

$temp_file = tempnam(sys_get_temp_dir(), 'db');

$repo = file_get_contents('https://arch-repo.gitlab.io/arch-repo-testing/archrepo.db.tar.xz');

file_put_contents($temp_file, $repo);

`mkdir -p $temp_file-dir`;
`tar xJf $temp_file -C $temp_file-dir`;

$iterator = new RecursiveIteratorIterator(
	new RecursiveDirectoryIterator("$temp_file-dir"),
	RecursiveIteratorIterator::CHILD_FIRST
);
$repo_packages = [];
foreach ($iterator as $file) {
	if (in_array($file->getBasename(), array('.', '..'))) {
		continue;
	} elseif ($file->isFile() || $file->isLink()) {
		// Process the json file.
		$file_name = $file->getPathname();
		$file_data = file_get_contents($file_name);
		$package = parse_file($file_data);
		$repo_packages[] = $package;
	}
}
foreach ($repo_packages as $package) {
	// Check if this is a base package.
	if ($package['BASE'] === $package['NAME'] && isset($packages[$package['BASE']])) {
		$packages[$package['BASE']]['repo'] = $package;
	}
}
// Cleanup
`rm -rf $temp_file-dir`;

$update_packages = [];
foreach ($packages as $package_base => $package) {
	if (!isset($package['aur'])) {
		$errors[] = "$package_base is no longer in the AUR.";
	} elseif ($package['aur']['Maintainer'] !== $package['maintainer']) {
		$errors[] = "$package_base is no longer maintained by {$package['maintainer']} but now by {$package['aur']['Maintainer']}.";
	} elseif (!isset($package['repo'])) {
		// Package is not in repo.
		$update_packages[] = $package;
	} elseif (version_compare($package['repo']['VERSION'], $package['aur']['Version'], '<')) {
		// Repo version is lower than AUR version.
		$update_packages[] = $package;
	}
}

if (!empty($update_packages)) {
	$package_to_update = $update_packages[mt_rand(0, count($update_packages) - 1)];

	$build_script = '#!/bin/bash'."\n\n";
	$build_script .= "set -e\n\n";
	if (!empty($package_to_update['gpg-keys'])) {
		foreach ($package_to_update['gpg-keys'] as $key) {
			$build_script .= 'sudo -u build gpg --recv-keys '.$key."\n";
		}
	}
	$build_script .= 'pacman -S --noconfirm --needed git';
	if (!empty($package_to_update['dependencies'])) {
		$build_script .= ' '.implode(' ', $package_to_update['dependencies']);
	}
	$build_script .= "\n";
	$build_script .= 'bash "$CI_PROJECT_DIR/scripts/build" '.$package_to_update['name'];
	if (!empty($package_to_update['makepkg'])) {
		$build_script .= ' "'.$package_to_update['makepkg'].'"';
	} else {
		$build_script .= ' "makepkg"';
	}
	$build_script .= "\n";
	file_put_contents('build_script', $build_script);
	echo "Doing build on package {$package_to_update['name']} with the following build script:\n";
	echo $build_script;
} elseif (!empty($errors)) {
	print_r($errors);
	exit(1);
}

function parse_file($data) {
	preg_match_all('/%([^%]+)%([^%]+)/', $data, $matches);
	$package = [];
	foreach ($matches[1] as $index => $key) {
		$package[$key] = trim($matches[2][$index]);
	}
	return $package;
}

